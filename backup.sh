#!/bin/bash

set -e

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source "$script_dir/snippets/exit-on-error/exit-on-error.sh"
source "$script_dir/snippets/process-json-config-data/process-json-config-data.sh"
config_data="$(cat $script_dir/backup-config.json)"
process_json_config_data archive_filename create_timestamp dest_dir max_stored_backups mount_cmd
echo "Mount command: $mount_cmd"

